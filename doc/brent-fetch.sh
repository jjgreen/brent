#!/bin/sh
#
# refresh brent.* from git master

NAME=brent
URL="https://gitlab.com/jjg/$NAME/raw/master"

for ext in c h
do
    FILE="$NAME.$ext"
    echo -n "$FILE .."
    wget -q $URL/$FILE -O $FILE
    echo ". done"
done
