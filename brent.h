/*
  brent.h
  Copyright (c) J.J. Green 2015
*/

#ifndef BRENT_H
#define BRENT_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdlib.h>

  typedef int (objective_t)(double, void*, double*);

  typedef struct
  {
    double tolerance;
    size_t max_iterations;
  } brent_opt_t;

  typedef struct
  {
    double minimum;
    size_t iterations, evaluations;
  } brent_stat_t;

  extern int brent(objective_t*, void*,
		   const double[static 3],
		   const brent_opt_t*,
		   double*,
		   brent_stat_t*);

#define BRENT_OK            0
#define BRENT_FAIL_MAXITER  1
#define BRENT_FAIL_EVAL     2
#define BRENT_FAIL_BRACKET  3
#define BRENT_FAIL_NPTOL    4

  extern const char* brent_strerror(int);

#ifdef __cplusplus
}
#endif

#endif
