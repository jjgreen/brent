Brent minimisation
==================

A C99 translation of the Brent minimisation routine from
Travis E. Oliphant's SciPy optimise module.
