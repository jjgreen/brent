/*
  cunit tests for brent_strerror.c
  J.J.Green 2015
*/

#include <math.h>
#include <brent.h>

#include "tests_brent_strerror.h"

CU_TestInfo tests_brent_strerror[] =
  {
    {"error-code non-negative",   test_brent_strerror_non_negative},
    {"unknown error has message", test_brent_strerror_non_null},
    {"error-code coverage",       test_brent_strerror_coverage},
    CU_TEST_INFO_NULL,
  };

static int codes[] = {
  BRENT_OK,
  BRENT_FAIL_MAXITER,
  BRENT_FAIL_EVAL,
  BRENT_FAIL_BRACKET,
  BRENT_FAIL_NPTOL
};

static int ncode = sizeof(codes)/sizeof(int);

extern void test_brent_strerror_non_negative(void)
{
  int i;

  for (i=0 ; i<ncode ; i++)
    CU_ASSERT(codes[i] >= 0);
}

extern void test_brent_strerror_non_null(void)
{
  CU_ASSERT_NOT_EQUAL(brent_strerror(-1), NULL);
}

extern void test_brent_strerror_coverage(void)
{
  int i;
  const char *unknown_code_msg = brent_strerror(-1);

  for (i=0 ; i<ncode ; i++)
    {
      const char *msg = brent_strerror(i);
      CU_ASSERT_STRING_NOT_EQUAL(msg, unknown_code_msg);
    }
}
