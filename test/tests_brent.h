/*
  brent.h
  Copyright (c) J.J. Green 2015
*/

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_brent[];
extern void test_brent_quadratic(void);
extern void test_brent_gaussian(void);
extern void test_brent_scipy(void);
extern void test_brent_bad_bracket(void);
extern void test_brent_bad_eval(void);
extern void test_brent_maxiter(void);
extern void test_brent_nptol(void);
