/*
  brent.c

  A C99 translation of the algorithm in SciPy's optimise.py by
  Travis E. Oliphant.  The original copyright notice reads:

  | You may copy and use this module as you see fit with no
  | guarantee implied provided you keep this notice in all copies.

  This translation is Copyright (c) J.J. Green 2015, released under
  the same terms.
*/

#include <math.h>
#include <stdbool.h>

#include "brent.h"

#define MAXITER 500
#define TOL     1.0e-8
#define MINTOL  1.0e-11
#define CG      0.3819660

static bool bracket_monotone_increasing(const double B[static 3])
{
  return (B[0] < B[1]) && (B[1] < B[2]);
}

extern int brent(objective_t *f,
		 void *arg,
		 const double bracket[static 3],
		 const brent_opt_t *opt,
		 double *x0,
		 brent_stat_t *stat)
{
  if (opt == NULL)
    {
      brent_opt_t default_opt = {
	.max_iterations = MAXITER,
	.tolerance = TOL
      };
      return brent(f, arg, bracket, &default_opt, x0, stat);
    }

  if (opt->tolerance <= 0.0)
    return BRENT_FAIL_NPTOL;

  if ( ! bracket_monotone_increasing(bracket) )
    return BRENT_FAIL_BRACKET;

  size_t neval = 0, niter = 0;
  double a, b, w, v, x, fw, fv, fx, dx = 0.0, rat = 0;

  a = bracket[0];
  w = v = x = bracket[1];
  b = bracket[2];

  if (f(x, arg, &fx) != 0)
    return BRENT_FAIL_EVAL;

  neval++;

  fw = fv = fx;

  while (true)
    {
      double
	tol1 = opt->tolerance * fabs(x) + MINTOL,
	tol2 = 2 * tol1,
	xmid = (a + b)/2;

      if (fabs(x - xmid) < (tol2 - (b - a)/2))
	break;

      if (fabs(dx) <= tol1)
	{
	  dx = (x < xmid ? b : a) - x;
	  rat = CG * dx;
	}
      else
	{
	  double
	    tmp1 = (x - w) * (fx - fv),
	    tmp2 = (x - v) * (fx - fw),
	    p = (x - v) * tmp2 - (x - w) * tmp1;

	  tmp2 = 2 * (tmp2 - tmp1);

	  if (tmp2 > 0.0)
	    p = -p;
	  else
	    tmp2 = -tmp2;

	  double dx_temp = dx;
	  dx = rat;

	  if (
	      (p > tmp2 * (a - x)) &&
	      (p < tmp2 * (b - x)) &&
	      (fabs(p) < fabs(0.5 * tmp2 * dx_temp))
	      )
	    {
	      rat = p * 1 / tmp2;
	      double u = x + rat;

	      if (((u - a) < tol2) || ((b - u) < tol2))
		{
		  rat = (xmid < x ? -tol1 : tol1);
		}
	    }
	  else
	    {
	      dx = (x < xmid ? b : a) - x;
	      rat = CG * dx;
	    }
	}

      double u, fu;

      if (fabs(rat) < tol1)
	u = x + (rat < 0 ? -tol1 : tol1);
      else
	u = x + rat;

      if (f(u, arg, &fu) != 0)
	return BRENT_FAIL_EVAL;

      neval++;

      if (fu > fx)
	{
	  if (u < x)
	    a = u;
	  else
	    b = u;

	  if ((fu <= fw) || (w == x))
	    {
	      v  = w;
	      w  = u;
	      fv = fw;
	      fw = fu;
	    }
	  else if ((fu <= fv) || (v == x) || (v == w))
	    {
	      v  = u;
	      fv = fu;
	    }
	}
      else
	{
	  if (u >= x)
	    a = x;
	  else
	    b = x;

	  v = w;
	  w = x;
	  x = u;
	  fv = fw;
	  fw = fx;
	  fx = fu;
	}

      if (++niter >= opt->max_iterations)
	return BRENT_FAIL_MAXITER;
    }

  if (stat != NULL)
    {
      stat->iterations = niter;
      stat->evaluations = neval;
      stat->minimum = fx;
    }

  *x0 = x;

  return BRENT_OK;
}

typedef struct
{
  int err;
  const char *msg;
} msg_entry_t;

extern const char* brent_strerror(int err)
{
  static const char nomsg[] = "unimplemented message";

  msg_entry_t *m, mtab[] =
    {
      { BRENT_OK,           "success" },
      { BRENT_FAIL_MAXITER, "maximum iterations exceeded" },
      { BRENT_FAIL_EVAL,    "objective function evaluation failed" },
      { BRENT_FAIL_BRACKET, "bad bracket argument supplied" },
      { BRENT_FAIL_NPTOL,   "non-positive tolerance option supplied" },
      { -1,                  NULL}
    };

  for (m = mtab ; m->msg ; m++)
    if (m->err == err)
      return m->msg;

  return nomsg;
}
